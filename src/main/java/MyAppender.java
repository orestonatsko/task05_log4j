
import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;

public class MyAppender{
    @Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
    public final class SmsAppender extends AbstractAppender {
        protected SmsAppender(String name, Filter filter,
                              Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
            super(name, filter, layout, ignoreExceptions);
        }

        public void append(LogEvent event) {
            try {
                Sms.send(new String(getLayout().toByteArray(event)));
            } catch (Exception ex) {
            }
        }

    }
}
